#include <stdlib.h>
#include <string.h>

#include "image.h"
#include "transformations.h"

static size_t matrix_to_array_position(size_t xmax, size_t x, size_t y) {
  return xmax * x + y;
}

struct image rotate90left(struct image image) {
  struct image result = {0};
  image_populate(&result, image.height, image.width);

  for (size_t row = 0; row < result.height; row++) {
    for (size_t column = 0; column < result.width; column++) {
      size_t resultIndex = matrix_to_array_position(result.width, row, column);
      size_t originalIndex = matrix_to_array_position(
          image.width, image.height - column, row - image.width);

      result.data[resultIndex].b = image.data[originalIndex].b;
      result.data[resultIndex].g = image.data[originalIndex].g;
      result.data[resultIndex].r = image.data[originalIndex].r;
    }
  }

  return result;
}
