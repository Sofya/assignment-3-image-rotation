#include <stdlib.h>

#include "bmp.h"

struct bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint16_t bfReserved1;
  uint16_t bfReserved2;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
} __attribute__((packed));

size_t get_bmp_padding(size_t width) { return width % 4; }

static void header_populate(struct bmp_header *header, uint32_t fileSize,
                            uint32_t width, uint32_t height) {
  header->bfType = (uint16_t)0x4D42;
  header->bfileSize = fileSize;
  header->bOffBits = sizeof(struct bmp_header);
  header->biSize = 40;
  header->biWidth = width;
  header->biHeight = height;
  header->biPlanes = 1;
  header->biBitCount = 24;
  header->biSizeImage =
      (uint32_t)((sizeof(struct pixel) * width) + get_bmp_padding(width)) *
      height;
}

bool to_bmp(FILE *file, struct image *image) {
  unsigned char zeros[64] = {0};
  if (!file || !image) {
    return false;
  }

  uint64_t bitmapSize =
      ((sizeof(struct pixel) * image->width) + get_bmp_padding(image->width)) *
      image->height;
  uint64_t fileSize = sizeof(struct bmp_header) + bitmapSize;

  if (bitmapSize >= INT32_MAX || fileSize >= INT32_MAX ||
      image->width >= INT32_MAX || image->height >= INT32_MAX) {
    return false;
  }

  long int pos_to_restore = ftell(file);

  struct bmp_header header = {0};
  header_populate(&header, (uint32_t)fileSize, (uint32_t)image->width,
                  (uint32_t)image->height);

  if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) {
    return false;
  }

  for (size_t row = 0; row < image->height; row++) {
    if (fwrite(image->data + (row * image->width), sizeof(struct pixel),
               image->width, file) != image->width)
      return false;
    if (fwrite(&zeros, get_bmp_padding(image->width), 1, file) != 1) {
      return false;
    }
  }

  fseek(file, pos_to_restore, SEEK_SET);
  return true;
}

bool from_bmp(FILE *file, struct image *image) {
  if (!file || !image) {
    return false;
  }

  long int pos_to_restore = ftell(file);
  struct bmp_header header;
  struct pixel pixel;

  fseek(file, 0, SEEK_SET);
  if (fread(&header, sizeof(struct bmp_header), 1, file) != 1) {
    fseek(file, pos_to_restore, SEEK_SET);
    return false;
  }

  if (header.bfType != (uint16_t)0x4D42) {
    fseek(file, pos_to_restore, SEEK_SET);
    return false;
  }

  image_populate(image, header.biWidth, header.biHeight);

  for (size_t row = 0; row < image->height; row++) {
    for (size_t column = 0; column < image->width; column++) {
      if (fread(&pixel, sizeof(struct pixel), 1, file) != 1) {
        free(image->data);
        image->height = 0;
        image->width = 0;
        image->data = NULL;
        fseek(file, pos_to_restore, SEEK_SET);
        return false;
      }
      image->data[row * image->width + column] = pixel;
    }
    if (fread(&pixel, get_bmp_padding(image->width), 1, file) != 1) {
      free(image->data);
      image->height = 0;
      image->width = 0;
      image->data = NULL;
      fseek(file, pos_to_restore, SEEK_SET);
      return false;
    }
  }

  fseek(file, pos_to_restore, SEEK_SET);
  return true;
}
