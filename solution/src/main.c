#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transformations.h"

int main(int argc, char **argv) {
  if (argc != 3) {
    puts("Wrong syntax");
    puts("Syntax: binary input output");
    return 1;
  }

  FILE *file_in = fopen(argv[1], "rb");
  if (!file_in) {
    puts("Input file access error");
    return 1;
  }

  FILE *file_out = fopen(argv[2], "wb");
  if (!file_out) {
    puts("Output file access error");
    return 1;
  }

  struct image original = {0};
  if (!from_bmp(file_in, &original)) {
    puts("File read error");
    return 1;
  }

  struct image rotated = rotate90left(original);

  if (!to_bmp(file_out, &rotated)) {
    puts("File write error");
    return 1;
  }

  free(original.data);
  free(rotated.data);

  fclose(file_in);
  fclose(file_out);

  return 0;
}

