#include "image.h"
#include <stdlib.h>

void image_populate(struct image *image, uint64_t width, uint64_t height) {
  image->width = width;
  image->height = height;
  image->data = malloc(width * height * sizeof(struct pixel));
}
