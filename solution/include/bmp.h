#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

size_t get_bmp_padding(size_t width);
bool to_bmp(FILE *file, struct image *image);
bool from_bmp(FILE *file, struct image *image);
