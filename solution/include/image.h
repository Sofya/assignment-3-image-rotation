#ifndef IMAGE
#define IMAGE
#include <stdint.h>

struct pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel *data;
};

void image_populate(struct image *image, uint64_t width, uint64_t height);

#endif
